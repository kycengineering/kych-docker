FROM node:10-alpine

COPY ./kych/ /var/www/kych
WORKDIR /var/www/kych/vue/kyc-hospitality-operations
RUN npm install --only=dev
RUN npm install
ENTRYPOINT npm run serve
