user root;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 2048;
	multi_accept on;
}

http {
	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=STATIC:10m inactive=24h max_size=1g;

	##
	# Basic Settings
	##
	sendfile on;
	keepalive_timeout 65;

	##
	# SSL Settings
	##
	ssl_certificate /etc/nginx/ssl/nginx.crt;
	ssl_certificate_key /etc/nginx/ssl/nginx.key;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

	##
	# Logging Settings
	##
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	##
	# Gzip Settings
	##
	gzip on;

	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}
