# KYC Docker Environment

*IMPORTANT - SYSTEM SETUP*

You must create a docker network that will support this compose environment.  Please run the command below.

```
docker network create --subnet=172.23.0.0/24 kycnetwork
```